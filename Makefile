# ascii -- interactive ASCII reference
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

CFLAGS = -O

PREFIX = /usr/local

.PHONY: all clean format cppcheck splint install uninstall
	version dist release refresh

all: ascii ascii.1

ascii: ascii.c splashscreen.h nametable.h
	$(CC) $(CFLAGS) -DREVISION='"$(VERS)"' ascii.c -o ascii

splashscreen.h: splashscreen
	sed <splashscreen >splashscreen.h \
		-e 's/\\/\\\\/g' \
		-e 's/"/\\"/g' \
		-e 's/.*/"&" "\\n"/'

nametable.h: nametable
	sed <nametable >nametable.h \
		-e '/^#/d' \
		-e 's/^[A-Za-z ]*: */    /' \
		-e 's/%%/    }, {/'

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

clean:
	rm -f ascii ascii.o splashscreen.h nametable.h
	rm -f *.rpm *.tar.gz MANIFEST *.1 *.html

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

CPPCHECKOPTS =
cppcheck: nametable.h splashscreen.h
	cppcheck -DREVISION='"$(VERS)"' $(CPPCHECKOPTS) ascii.c

SPLINT_SUPPRESSIONS = -boolops -nullret -initallelements +charintliteral
splint: nametable.h splashscreen.h
	splint +quiet +posixlib $(SPLINT_SUPPRESSIONS) ascii.c

install: ascii ascii.1
	cp ascii $(DESTDIR)$(PREFIX)/bin/ascii
	cp ascii.1 $(DESTDIR)$(PREFIX)/share/man/man1

uninstall:
	rm $(DESTDIR)$(PREFIX)/bin/ascii
	rm $(DESTDIR)$(PREFIX)/share/man/man1/ascii.1

SOURCES = \
	README COPYING NEWS.adoc control Makefile \
	ascii.c ascii.adoc splashscreen nametable

version:
	@echo $(VERS)

ascii-$(VERS).tar.gz: $(SOURCES) ascii.1
	@ls $(SOURCES) ascii.1 | sed s:^:ascii-$(VERS)/: >MANIFEST
	@(cd ..; ln -s ascii ascii-$(VERS))
	(cd ..; tar -czvf ascii/ascii-$(VERS).tar.gz `cat ascii/MANIFEST`)
	@(cd ..; rm ascii-$(VERS))

dist: ascii-$(VERS).tar.gz

release: ascii-$(VERS).tar.gz ascii.html
	shipper version=$(VERS) | sh -e -x

refresh: ascii.html
	shipper -N -w version=$(VERS) | sh -e -x
